﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Timers;

namespace PuppetVoice.Models
{
    public class HlsPlaylist : IHlsPlaylist, IDisposable
    {
        private const string silenceFileName = "https://streamdemoaudio.blob.core.windows.net/audio/Silence.mp3";

        public float TargetDuration { get; set; }
        public int MediaSequence { get; set; }
        public bool IsEnded { get; set; } = false;
        public bool IsPaused { get; set; }
        public string CurrentMedia { get; set; }

        public Queue<HlsSegment> Segments { get; set; } = new Queue<HlsSegment>();
        private Timer timer = new Timer();
        public HlsPlaylist()
        {
            timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (IsPaused)
            {
                this.TargetDuration = 9;
                this.MediaSequence++;
                Segments.Dequeue();
                Segments.Enqueue(new HlsSegment() { Duration = 9, FileName = silenceFileName });
                timer.Interval = 9000;
            }
            else
            {

                timer.Stop();
                PlayTrack(Program.Current.GetCurrent());
            }
        }

        public void Pause()
        {
            IsPaused = true;
          

        }

        public void PlayTrack(string filename)
        {
            this.MediaSequence++;
            if(Segments.Count > 0)
                Segments.Dequeue();

          
            var duration = 12;
            this.TargetDuration = duration;
            Segments.Enqueue(new HlsSegment() { FileName = filename, Duration = duration });
            timer.Interval = (int)duration*1000;
            timer.Start();
        }

        public void Resume()
        {
            timer.Stop();
            IsPaused = false;
            Segments.Dequeue();
            PlayTrack(Program.Current.GetNext());

        }

        public override string ToString()
        {
            string playlistString = "#EXTM3U";
            playlistString += Environment.NewLine + "#EXT-X-VERSION:4";
            playlistString += Environment.NewLine + $"#EXT-X-TARGETDURATION:{TargetDuration}";
            playlistString += Environment.NewLine + $"#EXT-X-MEDIA-SEQUENCE:{MediaSequence}";
            foreach (var segment in Segments)
            {
                playlistString += segment.ToString();
            }
            if (IsEnded)
                playlistString += $"{Environment.NewLine}#EXT-X-ENDLIST";

            return playlistString;
        }

        public void Dispose()
        {
            timer.Stop();
            Segments.Clear();
        }
    }
}
