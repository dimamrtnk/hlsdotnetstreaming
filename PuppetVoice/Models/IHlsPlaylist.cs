﻿using PuppetVoice.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PuppetVoice.Models
{
    public interface IHlsPlaylist: IDisposable
    {
        float TargetDuration { get; set; }
        int MediaSequence { get; set; }
        bool IsEnded { get; set; }
        bool IsPaused { get; set; }
        string CurrentMedia { get; set; }
        Queue<HlsSegment> Segments { get; set; }
        void PlayTrack(string filename);
        void Pause();
        void Resume();
        
    }
}
