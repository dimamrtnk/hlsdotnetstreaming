﻿using System;

namespace PuppetVoice.Models
{
    public class HlsSegment
    {
        public float Duration { get; set; }
        public string FileName { get; set; }
        public override string ToString()
        {
            return $"{Environment.NewLine}#EXTINF:{Duration},{Environment.NewLine}{FileName}";
        }
    }
}