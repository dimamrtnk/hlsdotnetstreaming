﻿using System;

namespace PuppetVoice.Models
{
    public class HlsByteRangeSegment : HlsSegment
    {
        public long Offset { get; set; }
        public long Length { get; set; }
        public override string ToString()
        {
            return $"{Environment.NewLine}#EXTINF:{Duration},{Environment.NewLine}#EXT-X-BYTERANGE:{Length}@{Offset}{Environment.NewLine}{FileName}";
        }
    }
}