﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Timers;
namespace PuppetVoice.Models
{
    public class HlsBytePlaylist : IHlsPlaylist
    {
        private const string silenceFileName = "https://streamdemoaudio.blob.core.windows.net/audio/Silence.mp3";
        private int bitrate = 128;// kb/sec 
        private int increment;
        private Timer timer = new Timer();
        private long totalBytes;
        public float TargetDuration { get; set; }
        public int MediaSequence { get; set; }
        public Queue<HlsSegment> Segments { get; set; } = new Queue<HlsSegment>();
        public bool IsEnded { get; set; } = false;
        public string CurrentMedia { get; set; }
        public bool IsPaused { get; set; }
        public HlsBytePlaylist()
        {

            this.TargetDuration = 1;
            increment = 3;
            timer.Interval = 1000;
            int oneSec = bitrate * 1024 / 8; //bitrate in bytes
            timer.Elapsed += (a, b) =>
            {
                this.MediaSequence++;
                if (!IsPaused)
                {
                    increment++;
                    this.Segments.Dequeue();
                    long length = oneSec;
                    if (((oneSec * increment) + oneSec) > totalBytes)//if file is almost played
                    {
                        length = ((oneSec * (increment + 1))) - totalBytes;//take the rest bytes
                        PlayTrack(Program.Current.GetNext());
                    }
                    this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = oneSec * increment, Length = length, Duration = 1, FileName = $"{this.CurrentMedia}" });
                }


            };

        }

        public void Pause()
        {
            timer.Stop();
            timer.Interval = 9000;
            int oneSec = bitrate * 1024 / 8; //bitrate in bytes



            this.Segments.Clear();
            this.MediaSequence += 4;
            this.TargetDuration = 9;
            this.Segments.Enqueue(new HlsSegment() { FileName = silenceFileName, Duration = 9 });
            IsPaused = true;
            timer.Start();
        }
        public void Resume()
        {
            timer.Stop();
            this.TargetDuration = 1;
            this.MediaSequence++;
            int oneSec = bitrate * 1024 / 8; //bitrate in bytes
            timer.Interval = 1000;
            // this.TargetDuration = 1;
            this.Segments.Clear();
            this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = oneSec * increment, Length = oneSec, FileName = this.CurrentMedia, Duration = 1 });
            increment++;
            this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = oneSec * increment, Length = oneSec, FileName = this.CurrentMedia, Duration = 1 });
            increment++;
            this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = oneSec * increment, Length = oneSec, FileName = this.CurrentMedia, Duration = 1 });
            increment++;
            this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = oneSec * increment, Length = oneSec, FileName = this.CurrentMedia, Duration = 1 });

            IsPaused = false;
            timer.Start();
        }
        public HlsBytePlaylist(string text)
        {
            throw new NotImplementedException();
        }

        public void PlayTrack(string filename)
        {
            timer.Stop();
            increment = 3;
            this.CurrentMedia = filename;
            this.Segments.Clear();

            //TODO:should retreive file size
            totalBytes = new WebClient().DownloadData(filename).Length;
            int oneSec = bitrate * 1024 / 8; //bitrate in bytes
                                             //I don't know how much segments should be sent at once. Maybe it should be more or less.
                                             //1 segment = 1 second of playback(i hope my calculations are correct)
            this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = 0, Length = oneSec, Duration = 1, FileName = $"{this.CurrentMedia}" });
            this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = oneSec, Length = oneSec, Duration = 1, FileName = $"{this.CurrentMedia}" });
            this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = oneSec * 2, Length = oneSec, Duration = 1, FileName = $"{this.CurrentMedia}" });
            this.Segments.Enqueue(new HlsByteRangeSegment() { Offset = oneSec * 3, Length = oneSec, Duration = 1, FileName = $"{this.CurrentMedia}" });

            timer.Start();


        }


        public static HlsBytePlaylist Parse(string text)
        {
            return new HlsBytePlaylist(text);
        }
        public override string ToString()
        {
            string playlistString = "#EXTM3U";
            playlistString += Environment.NewLine + "#EXT-X-VERSION:4";
            playlistString += Environment.NewLine + $"#EXT-X-TARGETDURATION:{TargetDuration}";
            playlistString += Environment.NewLine + $"#EXT-X-MEDIA-SEQUENCE:{MediaSequence}";
            foreach (var segment in Segments)
            {
                playlistString += segment.ToString();
            }
            if (IsEnded)
                playlistString += $"{Environment.NewLine}#EXT-X-ENDLIST";

            return playlistString;
        }

        public void Dispose()
        {

            timer.Stop();
            Segments.Clear();
        }
    }
   
}