﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using PuppetVoice.Models;

namespace PuppetVoice
{
    public class Program
    {
        private static Program instance = new Program();
        private static readonly object syncRoot = new Object();
        private int curAudio = -1;

        public IHlsPlaylist Playlist { get; set; } = new HlsPlaylist();

        private List<string> AudioFiles = new List<string>()
        {
            "https://streamdemoaudio.blob.core.windows.net/audio/FirstFile.mp3",
            "https://streamdemoaudio.blob.core.windows.net/audio/SecondFile.mp3",
            "https://streamdemoaudio.blob.core.windows.net/audio/ThirdFile.mp3",
            "https://streamdemoaudio.blob.core.windows.net/audio/FourthFile.mp3",
            "https://streamdemoaudio.blob.core.windows.net/audio/FifthFile.mp3",

        };
        public void Reset()
        {
            instance.Playlist.Dispose();
            instance.Playlist = new HlsPlaylist();
            curAudio = -1;
        }
        public string GetCurrent()
        {
            if (instance.curAudio != -1)
                return instance.AudioFiles[instance.curAudio];
            else return "Nothing is playing";
        }
        public string GetNext()
        {
            if (curAudio + 1 < instance.AudioFiles.Count)
            {
                curAudio++;
            }
            else
            {
                curAudio = 0;
            }
            return AudioFiles[curAudio];
        }
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static Program Current
        {
            get
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new Program();

                    return instance;
                }
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UseUrls("http://192.168.0.105:5000")
                .UseKestrel()
                .UseStartup<Startup>();
    }
}