﻿using Microsoft.AspNetCore.Mvc;
using PuppetVoice.Models;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace PuppetVoice.Controllers
{

    [Route("api/[controller]/[action]")]
    public class HlsController : Controller
    {
        private IHlsPlaylist playlist = Program.Current.Playlist;

        public HlsController()
        {
        }
        [HttpGet]
        [ActionName("getcurrentfile")]
        public string GetCurrentFile()
        {
            return Program.Current.GetCurrent();
        }
        [HttpGet]
        [ActionName("getstream")]
        public FileResult GetStream()
        {
            Console.WriteLine(playlist.ToString());
            return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(playlist.ToString())), "application/vnd.apple.mpegurl");
        }
        [HttpGet]
        [ActionName("playtrack")]
        public ActionResult PlayTrack()
        {
            playlist.PlayTrack(Program.Current.GetNext());
            return Redirect("/admin.html");
        }
        
        [HttpGet]
        [ActionName("pause")]
        public ActionResult Pause()
        {
            playlist.Pause();
            return Redirect("/admin.html");

        }
        [HttpGet]
        [ActionName("resume")]
        public ActionResult Resume()
        {
            playlist.Resume();
            return Redirect("/admin.html");

        }
        [HttpGet]
        [ActionName("end")]
        public ActionResult End()
        {
            playlist.IsEnded = true;
            return Redirect("/admin.html");

        }

        [HttpGet]
        [ActionName("reset")]
        public ActionResult Reset()
        {
            Program.Current.Reset();
            return Redirect("/admin.html");

        }
    }
}